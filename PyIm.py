#!/usr/bin/env python 

""" A tool intended for imaging and self-calibrating radio mosaic data with MIRIAD """

import sys 
import time
import datetime 
import argparse
from multiprocessing import Pool 
import generate_map 


class Params:
    def __init__(self):
        """   
        Params () 
        
        Initialises the parameter list with default values. These parameters 
        can be changed by accessing these class variables.
        """
        self.parallelise = False
        self.convol = False 
        self.alpha = False
        self.selfcal = False
        self.amp = False
        self.rms = False
        self.phase = False        
        self.stokes = "i"        
        self.robust = 0.0
        self.reg = "perc(66)"
        self.niters = 10000
        self.initsig = 0.0
        self.subsig = 0.0
        self.bins = 1
        self.refant = None 
        self.accept = 15.0


def main(): 
    parser = argparse.ArgumentParser(
        description="For imaging and self-calibrating radio mosaic data.")
    parser.add_argument("-parallelise", action="store_true",
                        help="Process multiple pointings. Use an addition with the\
                        start, step, and stop arguments [default: False]",
                        default=False)
    parser.add_argument("-convol", action="store_true",
                        help="Convolves the CLEAN map with the set -beam parameters\
                        to output an image of a desired resolution. Recommended to use\
                        this option if dealing wth sub-band images. [default: False]",
                        default=False)
    parser.add_argument("-alpha", action="store_true", 
                        help="Produce spectral index plane at image restoration\
                        stage. [Default: False]",
                        default=False)
    parser.add_argument("-selfcal", action="store_true",
                        help="Perform self-calibration. [default: False]",
                        default=False)
    parser.add_argument("-amp", action="store_true",
                        help="Perform amplitude selfcal. [default: False]",
                        default=False)
    parser.add_argument("-rms", action="store_true",
                        help="Assert selfcal improves CLEAN map. [default: False]",
                        default=False)
    parser.add_argument("-phase", action="store_true",
                        help="Assert RMS of phases after selfcal is better than the\
                        acceptable set value [default: False]",
                        default=False)
    parser.add_argument("-start", 
                        help="First pointing to image. [No default]",
                        type=int)
    parser.add_argument("-stop", 
                        help="Last pointing to image. [No default]",
                        type=int)
    parser.add_argument("-step", 
                        help="Pointing step. [No default]",
                        type=int)
    parser.add_argument("-freq", 
                        help="Central frequency of observation. Pointing\
                        directory is read as f_start.freq", type=int)
    parser.add_argument("-stokes", 
                        help="Stokes image of interest. [Default: i]",
                        type=str)
    parser.add_argument("-centre", 
                        help="Phase centre for pointings to lie on the\
                        same grid. Specify as HH:MM:SS.S,DD:MM:SS.S\
                        [Default: Use pointing's phase centre]",
                        type=str) 
    parser.add_argument("-imsize", 
                        help="Output image size. [No default]",
                        type=str)
    parser.add_argument("-cell", 
                        help="Image cell size (in arcsec). [No default]",
                        type=float) 
    parser.add_argument("-robust", 
                        help="Briggs robust weighting parameter. [default: 0.0]",
                        type=float)
    parser.add_argument("-reg", 
                        help="Region to deconvolve. [default: 'perc(66)']",
                        type=str) 
    parser.add_argument("-niters", 
                        help="Number of iterations for deconvolution.\
                        [default: 10000]",
                        type=int) 
    parser.add_argument("-initsig", 
                        help="Deconvolve dirty map down to INITISIG times\
                        the rms noise in the dirty map. [default: 0.0]",
                        type=float)
    parser.add_argument("-subsig", 
                        help="Clean self-calibrated dirty map down to SUBSIG times\
                        the rms noise in the previous CLEAN map. [default: 0.0]",
                        type=float)    
    parser.add_argument("-beam", 
                        help="Restoring beam size. Set as bmaj (arcsec) bmin\
                        (arcsec) bpa (deg). If sub-band imaging, this is used\
                        to smooth the CLEAN map (first produced by fitting a\
                        Gaussian to the main lobe of the dirty beam) to the\
                        specified resolution. [default: Fit a Gaussian to the\
                        main lobe of the dirty beam]",
                        type=float, nargs=3) 
    parser.add_argument("-solint", 
                        help="Time interval(s) for selfcal (in mins). [No default]",
                        type=float, nargs="+")
    parser.add_argument("-bins",
                        help="Frequency bins to compute selfcal solutions. [Default: 1]",
                        type=int)
    parser.add_argument("-refant", 
                        help="Reference antenna for selfcal. [default: Antenna with\
                        the greatest weight]",
                        type=int)
    parser.add_argument("-uvrange", 
                        help="UVrange to use for first selfcal only. Specify from\
                        shortest to longest baseline in units of kilo-lambda.\
                        Eg. 20,65 10,65 [Default: Full UVrange]",
                        type=str, nargs="+")
    parser.add_argument("-accept", 
                        help="The acceptable RMS of gain phases after selfcal beyond\
                        which the solutions are rejected (in degrees). [Default: 15.0]",
                        type=float)    
    parser.add_argument("datadir",
                        help="Data directory",
                        type=str)

    args = parser.parse_args()
    stokes = args.stokes or Params().stokes 
    robust = args.robust or Params().robust
    reg = args.reg or Params().reg 
    niters = args.niters or Params().niters
    initsig = args.initsig or Params().initsig
    subsig = args.subsig or Params().subsig
    bins = args.bins or Params().bins
    refant = args.refant or Params().refant
    accept = args.accept or Params().accept 

    print( "\nRequested Stokes image: ", stokes.upper())
    if args.phase:
        print("Assert RMS phases better than %s degree(s)" %accept)
    if args.rms:
        print("Requested to assert selfcal improves CLEAN map")
    if args.amp and len(args.solint)==1:
        print("Perform phase-only first before phase and amplitude!")
        sys.exit("Increase number of self-calibrations!")
    if args.convol:
        print("Sub-band imaging option activated ...")
        if args.beam is None:
            sys.exit("Set -beam as the resolution of the final output image!")
            
    # Mosaic option is used so set phase centre 
    if not args.centre:
        sys.exit(
            "\nSet the phase centre for all pointings to lie on the same grid!")        
    if not args.datadir:
        sys.exit("Set directory containing pointing folders!")
    if args.parallelise:
        # Combine datadir and freq to access pointing data
        # Each pointing data should be in datadir/f_start.freq/        
        if not args.freq:
            sys.exit("Set -freq which is the suffix of your pointing folders!")

        # Pointing fields
        if (args.start or args.step or args.step) is None:
            sys.exit("Set -start, -step ,and -stop!")
        if args.stop < args.start:
            sys.exit("-start must be less or equal to -stop!")        
        if (args.stop - args.start)== 0 and args.step > 1:
            sys.exit("Reduce -step to 1!")
        num_pnts = int((args.stop - args.start) / args.step) + 1 
        args.start = range(args.start, args.stop+1, args.step)
        print("Number of pointings = ", num_pnts)
    if args.selfcal:
        from selfcal import cal_and_image 
        if stokes == "i":
            print("\nPerform phase-only self-calibration")
        if not args.solint:
            sys.exit("Provide solution intervals using -solint!")
        print("%s round(s) of selfcal to be performed" %len(args.solint))
        if args.uvrange is None:
            print("Selfcal over full UVrange ...")
        else:
            print("SELFCAL over %s set(s) of UVrange ..." %len(args.uvrange))
            
    # Begin processing
    begin = time.time()
    if args.parallelise:    
        pool = Pool(processes=num_pnts)

        pool.starmap(generate_map.imaging, [(
            "%s/f_%s.%s" %(args.datadir, start, args.freq), stokes,
            args.imsize, args.cell, robust, args.centre, reg, initsig,
            niters, None, args.alpha, args.beam, args.convol) for start in args.start])

        if args.selfcal:
            pool.starmap(cal_and_image, [(
                "%s/f_%s.%s" %(args.datadir, start, args.freq), stokes, args.solint,
                args.uvrange, refant, bins, accept, args.imsize, args.cell, robust,
                args.centre, reg, subsig, niters, None, args.alpha, args.beam,
                args.amp, args.rms, args.phase, args.convol) for start in args.start])
        pool.close()            
    else:
        # Data directory must not be of the form datadir/f.start.freq/
        generate_map.imaging(args.datadir, stokes, args.imsize, args.cell, robust,
                             args.centre, reg, initsig, niters, None, args.alpha,
                             args.beam, args.convol)

        if args.selfcal:
            cal_and_image(args.datadir, stokes, args.solint, args.uvrange, refant,
                          bins, accept, args.imsize, args.cell, robust, args.centre,
                          reg, subsig, niters, None, args.alpha, args.beam, args.amp,
                          args.rms, args.phase, args.convol)
    
    end = time.time()
    print ("\nProcess finished in %s" %str(datetime.timedelta(seconds=end-begin)))

  
if __name__ == "__main__":
    main()
