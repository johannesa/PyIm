""" For producing an initial maps """

import os
import subprocess 
import numpy as np


def imaging(datadir, stokes, imsize, cell, robust, centre, reg, initsig, niters,
            prev_model, alpha, beam, convol): 
    """
    imaging(datadir, stokes, imsize, cell, robust, centre, reg, initsig, niters, 
            prev_model, alpha, beam, convol) 
        
    Inputs: 
          datadir(str)-   Directory containing the pointing data.
          stokes(str)-    Stokes image of interest. 
          imsize(int)-    Image size. 
          cell(float)-    Cell size. 
          robust(float)-  Briggs robust weighting. 
          centre(str)-    The phase centre.
          reg(str)-       Region to be deconvolved.
          initsig(float)- Sigma level for deconvolving initial dirty map. 
          niters(int)-    Maximum number of minor iterations during 
                          deconvolution.
          prev_model(boolean)-  An already existing MFCLEAN model. 
          alpha(boolean)- Option to create a spectral index map as an
                          extra plane to the CLEAN map. 
          beam(float)-    Restoring beam.
          convol(boolean)- Smooth CLEAN map to a desired resolution with 
                           beam parameters. 

    Outputs:
           1-  Dirty map and beam.
           2-  Model component map. 
           3-  MIRIAD CLEAN map of size -imsize.
    """
    psf = os.path.join(datadir, "_%sbeam" %stokes)
    dirty_map = os.path.join(datadir, "_%smap" %stokes)
    model_map = os.path.join(datadir, "_%smodel" %stokes)
    clean_map = os.path.join(datadir, "_%srestor" %stokes)
    if convol:
        convol_map = os.path.join(datadir, "_%sconvol" %stokes)
        
    # Perform inversion
    if not os.path.isdir(dirty_map):
        if (imsize and cell) is None:
            subprocess.run(["invert",
                            "vis=%s" %datadir,
                            "stokes=%s" %stokes,
                            "map=%s" %dirty_map,
                            "beam=%s" %psf,
                            "robust=%s" %robust,
                            "options=mfs,sdb,double,mosaic",
                            "offset=%s" %centre],
                           check=True)
        else:
            subprocess.run(["invert",
                            "vis=%s" %datadir,
                            "stokes=%s" %stokes,
                            "map=%s" %dirty_map,
                            "beam=%s" %psf,
                            "imsize=%s" %imsize,
                            "cell=%s" %cell,
                            "robust=%s" %robust,
                            "options=mfs,sdb,double,mosaic",
                            "offset=%s" %centre],
                           check=True)
                            
    # Perform deconvolution
    if not os.path.isdir(model_map):
        if prev_model is None:
            # Use RMS in dirty map to set CLEAN threshold 
            bash_cmd = "sigest in=%s > %s.log" %(dirty_map, dirty_map)
            subprocess.run(bash_cmd, shell=True, check=True)
            snr = initsig * float(
                open("%s.log" %dirty_map).readlines()[4].split()[-1])
            print ("\nMFCLEAN cut-off set to %.5f Jy ..." %snr)            
            subprocess.run(["mfclean",
                            "map=%s" %dirty_map,
                            "beam=%s" %psf,
                            "out=%s" %model_map,
                            "region=%s" %reg,
                            "cutoff=%s" %snr,
                            "niters=%s" %niters,
                            "log=%s/_mf.%slog" %(datadir, stokes)],
                           check=True)
        else:
            # Use RMS in previous CLEAN map to set CLEAN threshold
            snr = initsig * float(
                open(os.path.join(prev_model.split("_%smodel" %stokes)[0],
                                  "_%srms.log" %stokes)).readlines()[4].split()[-1])
            print ("\nMFCLEAN cut-off set to %.5f Jy ..." %snr)
            subprocess.run(["mfclean",
                            "map=%s" %dirty_map,
                            "beam=%s" %psf,
                            "model=%s" %prev_model,
                            "out=%s" %model_map,
                            "region=%s" %reg,
                            "cutoff=%s" %snr,
                            "niters=%s" %niters,
                            "log=%s/_mf.%slog" %(datadir, stokes)],
                           check=True)

        # Print information on the total CLEANed flux 
        try:
            flux = np.genfromtxt(
                "%s/_mf.%slog" %(datadir, stokes), usecols=3, invalid_raise=False)
            print ( "Total CLEANed flux: %.4f Jy" %np.sum(flux) )
        except ValueError:
            pass 

    # Perform restoration and/or smooth the CLEAN map 
    if convol:
        # First produce the CLEAN map
        if not os.path.isdir(clean_map):
            if alpha:
                subprocess.run(["restor",
                                "model=%s" %model_map,
                                "map=%s" %dirty_map,
                                "beam=%s" %psf,
                                "out=%s" %clean_map,
                                "options=mfs"],
                               check=True)
            else:
                subprocess.run(["restor",
                                "model=%s" %model_map,
                                "map=%s" %dirty_map,
                                "beam=%s" %psf,
                                "out=%s" %clean_map],
                               check=True)
        # Finally, smooth the CLEAN map
        if not os.path.isdir(convol_map):
            subprocess.run(["convol",
                            "map=%s" %clean_map,
                            "out=%s" %convol_map,
                            "fwhm=%s,%s" %(beam[0], beam[1]),
                            "pa=%s" %beam[2],
                            "options=final"],
                           check=True)

        # Determine the RMS in the smoothed CLEAN map
        bash_cmd = "sigest in=%s > %s/_%srms.log" %(convol_map, datadir, stokes)
        subprocess.run(bash_cmd, shell=True, check=True)
    else:
        if not os.path.isdir(clean_map):
            if beam is None:
                if alpha:
                    subprocess.run(["restor",
                                    "model=%s" %model_map,
                                    "map=%s" %dirty_map,
                                    "beam=%s" %psf,
                                    "out=%s" %clean_map,
                                    "options=mfs"],
                                   check=True)
                else:
                    subprocess.run(["restor",
                                    "model=%s" %model_map,
                                    "map=%s" %dirty_map,
                                    "beam=%s" %psf,
                                    "out=%s" %clean_map],
                                   check=True)
            else:
                if alpha: 
                    subprocess.run(["restor",
                                    "model=%s" %model_map,
                                    "map=%s" %dirty_map,
                                    "beam=%s" %psf,
                                    "out=%s" %clean_map,
                                    "fwhm=%s,%s" %(beam[0], beam[1]),
                                    "pa=%s" %beam[2],
                                    "options=mfs"],
                                   check=True)
                else:
                    subprocess.run(["restor",
                                    "model=%s" %model_map,
                                    "map=%s" %dirty_map,
                                    "beam=%s" %psf,
                                    "out=%s" %clean_map,
                                    "fwhm=%s,%s" %(beam[0], beam[1]),
                                    "pa=%s" %beam[2]],
                                   check=True)
                    
            # Determine the RMS in the CLEAN map 
            bash_cmd = "sigest in=%s > %s/_%srms.log" %(clean_map, datadir, stokes)
            subprocess.run(bash_cmd, shell=True, check=True)



        
