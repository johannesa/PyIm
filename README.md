## PyIm
`PyIm` is a python interface to MIRIAD written purposely for my Ph.D research - for imaging
the ATCA 2.1 GHz polarimetric mosaic data, but can be adapted for your own research. Any region
file for deconvolution should be saved into the data directory as `_cgcurs.region`.


## Features
- Fullband and Sub-band Imaging. Processing of multiple pointings is supported.
- Self-calibration with task `SELFCAL`. Both phase only, and phase and amplitude self-calibration are supported. For phase and amplitude self-calibration, the last solution interval provided is used.
- Multifrequency deconvolution with task `MFCLEAN`.



## Usage
```
usage: PyIm.py [-h] [-parallelise] [-convol] [-alpha] [-selfcal] [-amp] [-rms]
               [-phase] [-start START] [-stop STOP] [-step STEP] [-freq FREQ]
               [-stokes STOKES] [-centre CENTRE] [-imsize IMSIZE] [-cell CELL]
               [-robust ROBUST] [-reg REG] [-niters NITERS] [-initsig INITSIG]
               [-beam BEAM BEAM BEAM] [-solint SOLINT [SOLINT ...]]
               [-subsig SUBSIG] [-bins BINS] [-refant REFANT]
               [-uvrange UVRANGE [UVRANGE ...]] [-accept ACCEPT]
               datadir

For imaging and self-calibrating radio mosaic data. 

positional arguments:
  datadir               Data directory

optional arguments:
  -h, --help            show this help message and exit
  -parallelise          Process multiple pointings. Use an addition with the
                        start, step, and stop arguments [default: False]
  -convol               Convolves the CLEAN map with the set -beam parameters
                        to output an image of a desired resolution.
                        Recommended to use this option if dealing wth sub-band
                        images. [default: False]
  -alpha                Produce spectral index plane at image restoration
                        stage. [Default: False]
  -selfcal              Perform self-calibration. [default: False]
  -amp                  Perform amplitude selfcal. [default: False]
  -rms                  Assert selfcal improves CLEAN map. [default: False]
  -phase                Assert RMS of phases after selfcal is better than the
                        acceptable set value [default: False]
  -start START          First pointing to image. [No default]
  -stop STOP            Last pointing to image. [No default]
  -step STEP            Pointing step. [No default]
  -freq FREQ            Central frequency of observation. Pointing directory
                        is read as f_start.freq
  -stokes STOKES        Stokes image of interest. [Default: i]
  -centre CENTRE        Phase centre for pointings to lie on the same grid.
                        Specify as HH:MM:SS.S,DD:MM:SS.S [Default: Use
                        pointing's phase centre]
  -imsize IMSIZE        Output image size. [No default]
  -cell CELL            Image cell size (in arcsec). [No default]
  -robust ROBUST        Briggs robust weighting parameter. [default: 0.0]
  -reg REG              Region to deconvolve. [default: 'perc(66)']
  -niters NITERS        Number of iterations for deconvolution. [default:
                        10000]
  -initsig INITSIG      Deconvolve dirty map down to INITISIG times the rms
                        noise in the dirty map. [default: 0.0]
  -beam BEAM BEAM BEAM  Restoring beam size. Set as bmaj (arcsec) bmin
                        (arcsec) bpa (deg). If sub-band imaging, this is used
                        to smooth the CLEAN map (first produced by fitting a
                        Gaussian to the main lobe of the dirty beam) to the
                        specified resolution. [default: Fit a Gaussian to the
                        main lobe of the dirty beam]
  -solint SOLINT [SOLINT ...]
                        Time interval(s) for selfcal (in mins). [No default]
  -subsig SUBSIG        Clean self-calibrated dirty map down to SUBSIG times
                        the rms noise in the previous CLEAN map. [default:
                        0.0]
  -bins BINS            Frequency bins to compute selfcal solutions. [Default:
                        1]
  -refant REFANT        Reference antenna for selfcal. [default: Antenna with
                        the greatest weight]
  -uvrange UVRANGE [UVRANGE ...]
                        UVrange to use for first selfcal only. Specify from
                        shortest to longest baseline in units of kilo-lambda.
                        Eg. 20,65 10,65 [Default: Full UVrange]
  -accept ACCEPT        The acceptable RMS of gain phases after selfcal beyond
                        which the solutions are rejected (in degrees).
                        [Default: 15.0]

```


## Help
If you find a bug in this tool, please [submit a ticket](https://gitlab.com/johannesa/PyIm/-/issues)
and it will be attended to. 