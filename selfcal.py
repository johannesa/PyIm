""" For performing phase only or phase and amplitude self-calibration """

import re 
import os
import subprocess
import numpy as np
import generate_map


def check_phase(logfile, bins):
    """ 
    check_phase(logfile, bins)

    Reads phase RMS after selfcal from file.

    Inputs:
          logfile(textfile)- Logger from which the phase RMS 
                             is read. 
          bins(int)- Number of bins over which selfcal 
                     solutions should be determined.
    
    Outputs:
           RMS of phase after selfcal.
    """
    flines = open(logfile).readlines()
    line = [entry for entry in flines if re.search(
        "SELFCAL: Rms of the gain phase", entry, re.IGNORECASE)]
    if bins == 1:
        rms_phase = float(line[-1].split(":")[2])
    else:
        rms_phase = float(line[-(bins + 1)].split(":")[2])
    return rms_phase


def check_rms(initial_logfile, recent_logfile): 
    """ 
    check_rms(initial_logfile, recent_logfile)

    Reads RMS in CLEAN maps from a logfile.

    Inputs:
          initial_logfile(textfile)- Logger from which the RMS of the 
                                     initial map is read. 
          recent_logfile(textfile)- Logger from which the RMS of the 
                                    self-calibrated map is read. 

    Outputs: 
           1-  RMS in the initial map.
           2-  RMS in self-calibrated map.
    """
    initial_rms = open(initial_logfile).readlines()[4].split()[-1]
    recent_rms = open(recent_logfile).readlines()[4].split()[-1]
    return float(initial_rms), float(recent_rms)


def write_tofile(info): 
    """ 
    write_tofile(info)
    
    Write failed pointing information to file.

    Inputs:
          info(txt)- Information about failed selfcal or selfcal 
                     failing to improve the RMS in a map. 

    Outputs:
           Writes the information to a text file for 
           investigation by the user.
    """
    new_file = open("%s/failed_pointings.txt" %os.getcwd(), "a+")
    new_file.write("%s\n" %info) 
    new_file.close() 

    
def cal_and_image(datadir, stokes, solint, uvrange, refant, bins, accept,
                  imsize, cell, robust, centre, reg, subsig, niters,
                  prev_model, alpha, beam, amp, rms, phase, convol):
    """ 
    cal_and_image(datadir, stokes, solint, uvrange, refant, bins, accept, 
                  imsize, cell, robust, centre, reg, subsig, niters, 
                  prev_model, alpha, beam, amp, rms, phase, convol)
    
    Performs phase or phase and amp self-calibration. 

    Inputs:
          datadir(str)- Directory the containing pointing data.
          stokes(str)- Stokes image of interest. 
          solint(float)- Solution interval (in minutes). 
          uvrange(str)- UVrange over which selfcal should be 
                        performed.
          refant(int)- Reference antenna.
          bins(int)- Number of bins to determine gains solutions. 
          accept(float)- RMS of phases beyond which the gain 
                         solutions should be rejected.
          imsize(int)- Image size (in arcsec).
          cell(float)- Cell size (in arcsec).
          robust(float)- Briggs robust weighting.
          centre(str)-  The phase centre.
          reg(str)- Region to be deconvolved. 
          subsig(float)- Sigma level for CLEANing after selfcal. 
          niters(int)- Maximum number of minor iterations for 
                       deconvolution.  
          prev_model(boolean)-  An already existing MFCLEAN model.   
          alpha(boolean)- Option to create a spectral index map as 
                          an extra plane to the CLEAN map. 
          beam(float)- Restoring beam size.  
          amp(boolean)- Perform amplitude self-calibration.
          rms(boolean)-  Assert self-calibration improves the CLEAN map.
          phase(boolean)- Assert RMS of phases after self-calibration
                          is better than the acceptable phases.
          convol(boolean)- Smooth CLEAN map to a desired resolution with
                           beam parameters. 

    Outputs:
           1- Gain solutions.
           2- Self-calibrated visibilities
           3- Self-calibrated CLEAN map. 
    """
    pnt = os.path.normpath(datadir).split("/")[-1].split(".")[0]
    if os.path.isfile("%s/_cgcurs.region" %datadir):
        reg = "%s,@%s/_cgcurs.region" %(reg, os.path.normpath(datadir))
    
    if uvrange is None:
        #==============================
        # SELFCAL OVER FULL UVRANGE 
        #===============================        
        for idx,val in enumerate(solint):
            if not os.path.isdir("%s/0%s_selfcal/" %(datadir, idx+1)):
                subprocess.run(["mkdir",
                                "%s/0%s_selfcal/" %(datadir, idx+1)],
                               check=True)
                subprocess.run(["cp",
                                "%s/visdata" %datadir,
                                "%s/vartable" %datadir,
                                "%s/history" %datadir,
                                "%s/header" %datadir,
                                "%s/flags" %datadir,
                                "%s/0%s_selfcal" %(datadir, idx+1)],
                               check=True)
            # Selfcal iff the gain solutions do not exist
            if not os.path.isfile(
                    "%s/0%s_selfcal/gains" %(datadir, idx+1)) and stokes=="i":
                print("\nSELFCAL No. %s begins: %s min(s) ..." %(idx+1, val))
                if idx == 0:
                    sky_model = os.path.join(datadir, "_%smodel" %stokes)
                    pix_val = max(np.genfromtxt("%s/_mf.%slog" %(datadir, stokes),
                                                invalid_raise=False, usecols=3))
                else:
                    sky_model = os.path.join(datadir, "0%s_selfcal" %idx, "_%smodel" %stokes)
                    pix_val = max(
                        np.genfromtxt("%s/0%s_selfcal/_mf.%slog" %(datadir, idx, stokes),
                                      invalid_raise=False, usecols=3))
                if pix_val < 0:
                    pix_val = 0
                if refant is None:
                    if amp and idx==len(solint)-1:
                        subprocess.run(["selfcal",
                                        "vis=%s" %datadir, 
                                        "model=%s" %sky_model,
                                        "clip=%s" %pix_val,
                                        "interval=%s" %(solint[len(solint)-1]),
                                        "nfbin=%s" %bins,
                                        "options=mfs,amplitude"],
                                       check=True)
                    else:
                        subprocess.run(["selfcal",
                                        "vis=%s" %datadir,
                                        "model=%s" %sky_model,
                                        "clip=%s" %pix_val,
                                        "interval=%s" %val,
                                        "nfbin=%s" %bins,
                                        "options=mfs,phase"],
                                       check=True)
                else:
                    if amp and idx==len(solint)-1:
                        subprocess.run(["selfcal",
                                        "vis=%s" %datadir, 
                                        "model=%s" %sky_model,
                                        "clip=%s" %pix_val,
                                        "interval=%s" %(solint[len(solint)-1]),
                                        "refant=%s" %refant,
                                        "nfbin=%s" %bins,
                                        "options=mfs,amplitude"],
                                       check=True)
                    else:
                        subprocess.run(["selfcal",
                                        "vis=%s" %datadir,
                                        "model=%s" %sky_model,
                                        "clip=%s" %pix_val,
                                        "interval=%s" %val,
                                        "refant=%s" %refant,
                                        "nfbin=%s" %bins,
                                        "options=mfs,phase"],
                                       check=True)
                # Save history and copy gain solutions
                subprocess.run(["prthis",
                                "in=%s" %datadir,
                                "log=%s/0%s_selfcal/_prthis.log"
                                %(datadir, idx+1)], check=True)
                subprocess.run(["gpcopy",
                                "vis=%s" %datadir,
                                "out=%s/0%s_selfcal" %(datadir, idx+1)],
                               check=True)
                if bins == 1:
                    subprocess.run(["rm",
                                    "%s/gains" %datadir], check=True) 
                else:
                    subprocess.run(["rm",
                                    "%s/gains" %datadir,
                                    "%s/gainsf" %datadir], check=True)
            if phase:
                # Assert acceptable RMS before re-imaging 
                rms_phase = check_phase("%s/0%s_selfcal/_prthis.log" %(datadir, idx+1), bins)
                if rms_phase > accept:
                    print("SELFCAL No. %s on %s failed!" %(idx+1, pnt))
                    write_tofile("SELFCAL No. %s on %s failed" %(idx+1, pnt))
                    break 
            # Re-image self-calibrated data
            if idx==0:
                prev_model = os.path.join(datadir, "_%smodel" %stokes)
            else:
                prev_model = os.path.join(datadir, "0%s_selfcal" %idx, "_%smodel" %stokes)
            datadir = os.path.join(datadir,
                                   "0%s_selfcal" %(idx+1))
            generate_map.imaging(datadir, stokes, imsize, cell, robust, centre, reg,
                                 subsig, niters, prev_model, alpha, beam, convol)
            if rms:
                # Assert the selfcal improved the map
                if idx==0:
                    initial_file = os.path.join(os.path.split(datadir)[0],
                                                "_%srms.log" %stokes)
                else:
                    initial_file = os.path.join(os.path.split(datadir)[0],
                                                "0%s_selfcal/_%srms.log" %(idx, stokes))
                recent_file = "%s/_%srms.log" %(datadir, stokes)
                initial_rms, recent_rms = check_rms(initial_file, recent_file)
                if recent_rms >= initial_rms:
                    print(
                        "SELFCAL No. %s failed to improve Stokes %s map!" %(idx+1, stokes.upper()))
                    write_tofile(
                        "SELFCAL No. %s failed to improve Stokes %s map!" %(idx+1, stokes.upper())) 
                    break
            # Pass directory containing initial maps for the next selfcal to work
            datadir = os.path.split(datadir)[0]
    else:
        #==================================
        # SELFCAL OVER SELECTED UVRANGE
        #==================================
        for idx,val in enumerate(solint):
            if idx==0:
                for j,uv in enumerate(uvrange):
                    if not os.path.isdir("%s/0%s_selfcal/0%s/"
                                         %(datadir, idx+1, j+1)):
                        os.makedirs("%s/0%s_selfcal/0%s/" %(datadir, idx+1, j+1))
                        subprocess.run(["cp",
                                        "%s/visdata" %datadir, 
                                        "%s/vartable" %datadir,
                                        "%s/history" %datadir,
                                        "%s/header" %datadir,
                                        "%s/flags" %datadir,
                                        "%s/0%s_selfcal/0%s" %(datadir, idx+1, j+1)],
                                       check=True)
                    # Selfcal iff the gain solutions do not exist
                    if not os.path.isfile("%s/0%s_selfcal/0%s/gains" %(
                            datadir, idx+1, j+1)) and stokes=="i":
                        print("\nSELFCAL No. %s begins: %s min(s) over %s k-lambda ..." %(
                            idx+1, val, uv))
                        if j==0:
                            sky_model = os.path.join(datadir, "_%smodel" %stokes)
                            pix_val = max(np.genfromtxt("%s/_mf.%slog" %(datadir, stokes),
                                                        invalid_raise=False, usecols=3))
                        else:
                            sky_model = os.path.join(datadir, "0%s_selfcal" %(idx+1),
                                                     "0%s" %j, "_%smodel" %stokes)
                            pix_val = max(np.genfromtxt("%s/0%s_selfcal/0%s/_mf.%slog" %(
                                datadir, idx+1, j, stokes), invalid_raise=False, usecols=3))
                        if pix_val < 0:
                            pix_val = 0
                        if refant is None:
                            subprocess.run(["selfcal",
                                            "vis=%s" %datadir,
                                            "'select=uvrange(%s)'" %uv,
                                            "model=%s" %sky_model,
                                            "clip=%s" %pix_val,
                                            "interval=%s" %val,
                                            "nfbin=%s" %bins,
                                            "options=mfs,phase"],
                                           check=True)
                        else:
                            subprocess.run(["selfcal",
                                            "vis=%s" %datadir,
                                            "'select=uvrange(%s)'" %uv,
                                            "model=%s" %sky_model,
                                            "clip=%s" %pix_val,
                                            "interval=%s" %val,
                                            "refant=%s" %refant,
                                            "nfbin=%s" %bins,
                                            "options=mfs,phase"],
                                           check=True)
                        subprocess.run(["prthis",
                                        "in=%s" %datadir,
                                        "log=%s/0%s_selfcal/0%s/_prthis.log"
                                        %(datadir, idx+1, j+1)],
                                       check=True)
                        subprocess.run(["gpcopy",
                                        "vis=%s" %datadir,
                                        "out=%s/0%s_selfcal/0%s"
                                        %(datadir, idx+1, j+1)], check=True)
                        if bins==1:
                            subprocess.run(["rm",
                                            "%s/gains" %datadir], check=True)
                        else:
                            subprocess.run(["rm",
                                            "%s/gains" %datadir,
                                            "%s/gainsf" %datadir], check=True)
                    if phase and j==len(uvrange)-1:
                        # Assert acceptable RMS after selfcal before re-imaging
                        rms_phase = check_phase(
                            "%s/0%s_selfcal/0%s/_prthis.log" %(datadir, idx+1, j+1), bins)
                        if rms_phase > accept:
                            print("SELFCAL No. %s over %s k-lambda on %s failed!"  %(idx+1, uv, pnt))
                            write_tofile("SELFCAL No. %s over %s k-lambda on %s failed!" %(idx+1, uv, pnt))
                            break
                    # Re-image self-calibrated data 
                    if j==0:
                        prev_model = os.path.join(datadir, "_%smodel" %stokes)
                    else:
                        prev_model = os.path.join(datadir, "0%s_selfcal/0%s/_%smodel"
                                                  %(idx+1, j, stokes))
                    datadir = os.path.join(datadir, "0%s_selfcal" %(idx+1), "0%s" %(j+1))
                    generate_map.imaging(datadir, stokes, imsize, cell, robust, centre,
                                         reg, subsig, niters, prev_model, alpha, beam, convol)
                    if rms and j==len(uvrange)-1:
                        # Assert the selfcal improved the map
                        initial_file = os.path.join(os.path.split(os.path.split(datadir)[0])[0],
                                                    "_%srms.log" %stokes)
                        recent_file = "%s/_%srms.log" %(datadir, stokes)
                        initial_rms, recent_rms = check_rms(initial_file, recent_file)                            
                        if recent_rms >= initial_rms:
                            print(
                                "SELFCAL No. %s over %s k-lambda on %s failed to improve Stokes %s map!" %(
                                    idx+1, uvrange[len(uvrange)-1], pnt, stokes.upper()))
                            write_tofile(
                                "SELFCAL No. %s over %s k-lambda on %s failed to improve Stokes %s map!" %(
                                    idx+1, uvrange[len(uvrange)-1], pnt, stokes.upper()))
                            break
                    # Pass the directory containing the initial maps for the selfcal to work
                    datadir = os.path.split(os.path.split(datadir)[0])[0]
            else:
                # Assert acceptable phase and RMS before proceeding to subsequent selfcal
                if phase and idx==1:
                    rms_phase = check_phase("%s/0%s_selfcal/0%s/_prthis.log" %(datadir, idx, j+1), bins)
                    if rms_phase > accept:
                        break 
                if rms and idx==1:
                    initial_file = os.path.join(datadir, 
                                                "_%srms.log" %stokes)
                    recent_file = os.path.join("%s/0%s_selfcal/0%s" %(datadir, idx, j+1),
                                               "_%srms.log" %stokes)
                    initial_rms, recent_rms = check_rms(initial_file, recent_file)
                    if recent_rms >= initial_rms:
                        break 
                if not os.path.isdir("%s/0%s_selfcal/" %(datadir, idx+1)):
                    subprocess.run(["mkdir",
                                    "%s/0%s_selfcal/" %(datadir, idx+1)],
                                   check=True)
                    subprocess.run(["cp",
                                    "%s/visdata" %datadir,
                                    "%s/vartable" %datadir,
                                    "%s/history" %datadir,
                                    "%s/header" %datadir,
                                    "%s/flags" %datadir,
                                    "%s/0%s_selfcal" %(datadir, idx+1)],
                                   check=True)
                # Selfcal iff the gain solutions do not exist 
                if not os.path.isfile(
                        "%s/0%s_selfcal/gains" %(datadir, idx+1)) and stokes=="i":
                    print("\nSELFCAL No. %s begins: %s min(s) over %s k-lambda ..."
                          %(idx+1, val, uvrange[j]))
                    if idx==1:
                        sky_model = os.path.join(datadir,
                                                 "0%s_selfcal" %idx,
                                                 "0%s" %(j+1), 
                                                 "_%smodel" %stokes)
                        pix_val = max(np.genfromtxt("%s/0%s_selfcal/0%s/_mf.%slog"
                                                    %(datadir, idx, j+1, stokes),
                                                    invalid_raise=False, usecols=3))
                    else:
                        sky_model = os.path.join(datadir,
                                                 "0%s_selfcal" %idx,
                                                 "_%smodel" %stokes)
                        pix_val = max(np.genfromtxt("%s/0%s_selfcal/_mf.%slog"
                                                    %(datadir, idx, stokes),
                                                    invalid_raise=False, usecols=3))
                    if pix_val < 0:
                        pix_val = 0
                    if refant is None:
                        if amp and idx==len(solint)-1:
                            subprocess.run(["selfcal",
                                            "vis=%s" %datadir,
                                            "'select=uvrange(%s)'" %uvrange[j],
                                            "model=%s" %sky_model,
                                            "clip=%s" %pix_val,
                                            "interval=%s" %(solint[len(solint)-1]),
                                            "nfbin=%s" %bins,
                                            "options=mfs,amplitude"],
                                           check=True)
                        else:
                            subprocess.run(["selfcal",
                                            "vis=%s" %datadir,
                                            "'select=uvrange(%s)'" %uvrange[j], 
                                            "model=%s" %sky_model,
                                            "clip=%s" %pix_val,
                                            "interval=%s" %val,
                                            "nfbin=%s" %bins,
                                            "options=mfs,phase"],
                                           check=True)
                    else:
                        if amp and idx==len(solint)-1:
                            subprocess.run(["selfcal",
                                            "vis=%s" %datadir,
                                            "'select=uvrange(%s)'" %uvrange[j], 
                                            "model=%s" %sky_model,
                                            "clip=%s" %pix_val,
                                            "interval=%s" %(solint[len(solint)-1]),
                                            "refant=%s" %refant,
                                            "nfbin=%s" %bins,
                                            "options=mfs,amplitude"],
                                           check=True)
                        else:
                            subprocess.run(["selfcal",
                                            "vis=%s" %datadir,
                                            "'select=uvrange(%s)'" %uvrange[j], 
                                            "model=%s" %sky_model,
                                            "clip=%s" %pix_val,
                                            "interval=%s" %val,
                                            "refant=%s" %refant,
                                            "nfbin=%s" %bins,
                                            "options=mfs,phase"],
                                           check=True)
                    subprocess.run(["prthis",
                                    "in=%s" %datadir,
                                    "log=%s/0%s_selfcal/_prthis.log" %(datadir, idx+1)],
                                   check=True)
                    subprocess.run(["gpcopy",
                                    "vis=%s" %datadir,
                                    "out=%s/0%s_selfcal" %(datadir, idx+1)],
                                   check=True)
                    if bins==1:
                        subprocess.run(["rm",
                                        "%s/gains" %datadir], check=True)
                    else:
                        subprocess.run(["rm",
                                        "%s/gains" %datadir,
                                        "%s/gainsf" %datadir], check=True)
                if phase:
                    # Assert acceptable phase before re-imaging 
                    rms_phase = check_phase(
                        "%s/0%s_selfcal/_prthis.log" %(datadir, idx+1), bins)
                    if rms_phase > accept:
                        print("SELFCAL No. %s over %s k-lambda on %s failed!" %(
                                idx+1, uvrange[j], pnt))
                        write_tofile("SELFCAL No. %s over %s k-lambda on %s failed!"
                                     %(idx+1, uvrange[j], pnt))
                        break
                # Re-image self-calibrated data
                if idx==1:
                    prev_model = os.path.join(datadir,
                                              "0%s_selfcal" %idx,
                                              "0%s" %(len(uvrange)),
                                              "_%smodel" %stokes)
                else:
                    prev_model = os.path.join(datadir,
                                              "0%s_selfcal" %idx,
                                              "_%smodel" %stokes)
                datadir = os.path.join(datadir,
                                       "0%s_selfcal" %(idx+1))
                generate_map.imaging(datadir, stokes, imsize, cell, robust, centre,
                                     reg, subsig, niters, prev_model, alpha, beam, convol)
                if rms:
                    # Assert selfcal improved the map 
                    if idx==1:
                        initial_file = os.path.join(os.path.split(datadir)[0],
                                                    "0%s_selfcal/0%s/_%srms.log" %(idx, j+1, stokes))
                    else:
                        initial_file = os.path.join(os.path.split(datadir)[0],
                                                    "0%s_selfcal/_%srms.log" %(idx, stokes))
                    recent_file = "%s/_%srms.log" %(datadir, stokes)
                    initial_rms, recent_rms = check_rms(initial_file, recent_file)
                    if recent_rms >= initial_rms:
                        print("SELFCAL No. %s over %s k-lambda on %s failed to improve Stokes %s map!" %(
                            idx+1, uvrange[len(uvrange)-1], pnt, stokes.upper()))
                        write_tofile("SELFCAL No. %s over %s k-lambda on %s failed to improve Stokes %s map!" %(
                            idx+1, uvrange[len(uvrange)-1], pnt, stokes))
                        break 
                # Pass the directory containing the initial maps for the selfcal to work
                datadir = os.path.split(datadir)[0]

